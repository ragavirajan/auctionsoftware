json.extract! product, :id, :product_title, :user_id, :category_id, :created_at, :updated_at
json.url product_url(product, format: :json)
