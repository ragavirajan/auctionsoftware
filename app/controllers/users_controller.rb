class UsersController < ApplicationController
	def sign_up
		@user = User.new
	end

	def register
		 @user = User.new(user_params)
		 if @user.save
    	   	 redirect_to login_path
		 end
	end

	def home
		
	end

	def category
		@category = Category.new
	end

	def category_save
		data = Category.new(category_name: params[:category][:category_name])
        if data.save
		 redirect_to users_home_path
	    end
	end

	private
	def user_params
		params.require(:user).permit!
		
	end
end
