Rails.application.routes.draw do
  resources :products
root to: 'users#sign_up'

  resources :sessions, only: [:new, :create, :destroy]  
  get 'session_login' ,to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
get 'users/sign_up'
post 'users/register'
get 'users/home'
get 'users/category'
post 'users/category_save'

end
